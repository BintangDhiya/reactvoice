module.exports = {
  root: true,
  extends: '@react-native-community',
  plugins: '@typescript-eslint/eslint-plugin'
};
