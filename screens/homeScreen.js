import React from 'react';
import {
    SafeAreaView,
    ActivityIndicator,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Button,
    Alert,
    Image,
} from 'react-native';

export class HomeScreen extends React.Component {
    render() {
        const {navigate} = this.props.navigation;
        return (
            <SafeAreaView>
                <ActivityIndicator size="large" color="#0000ff"/>
                <Button
                title="Record something"
                onPress={() => navigate('Record')}
                />
            </SafeAreaView>

        );
    }
}
