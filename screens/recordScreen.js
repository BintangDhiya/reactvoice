import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Button,
    Alert,
    TouchableHighlight
} from 'react-native';
import {AudioRecorder, AudioUtils} from 'react-native-audio';
import Sound from "react-native-sound";

let AudioPath = AudioUtils.DocumentDirectoryPath + '/test.aac';


// import {Player, Recorder, MediaStates} from "@react-native-community/audio-toolkit";

// import AudioRecorderPlayer from 'react-native-audio-recorder-player';


export class RecordScreen extends React.Component {
    static navigationOptions = {
        title: 'Welcome',
    };

    constructor(props) {
        super(props);
        this.state = {
            isRecording: false,
            isRecordingFinished: false,
            currentTime: 0.0,
            recording: false,
            paused: false,
            stoppedRecording: false,
            finished: false,
            audioPath: AudioUtils.DocumentDirectoryPath + '/test.aac',
            hasPermission: undefined,
        }
        console.log('RecordScreen -> constructor');
    }

    componentDidMount() {
        AudioRecorder.requestAuthorization().then((isAuthorised) => {
            this.setState({hasPermission: isAuthorised});

            if (!isAuthorised) return;

            this.prepareRecordingPath(AudioPath);

            AudioRecorder.onProgress = (data) => {
                this.setState({currentTime: Math.floor(data.currentTime)});
            };

            AudioRecorder.onFinished = (data) => {
                // Android callback comes in the form of a promise instead.
                if (Platform.OS === 'ios') {
                    this._finishRecording(data.status === "OK", data.audioFileURL, data.audioFileSize);
                }
            };
        });
    }

    async _record() {
        if (this.state.recording) {
            console.warn('Already recording!');
            return;
        }

        if (!this.state.hasPermission) {
            console.warn('Can\'t record, no permission granted!');
            return;
        }

        if (this.state.stoppedRecording) {
            this.prepareRecordingPath(this.state.audioPath);
        }

        this.setState({recording: true, paused: false});

        try {
            const filePath = await AudioRecorder.startRecording();
        } catch (error) {
            console.error(error);
        }
    }

    _finishRecording(didSucceed, filePath, fileSize) {
        this.setState({isRecordingFinished: didSucceed});
        console.log(`Finished recording of duration ${this.state.currentTime} seconds at path: ${filePath} and size of ${fileSize || 0} bytes`);
    }

    prepareRecordingPath(audioPath) {
        AudioRecorder.prepareRecordingAtPath(audioPath, {
            SampleRate: 22050,
            Channels: 1,
            AudioQuality: "Low",
            AudioEncoding: "aac",
            AudioEncodingBitRate: 32000
        });
    }

    voiceRender = async () => {
        let result = await this.voiceRenderProcessor();
        console.log({voiceRender: result}, 'RecordScreen -> voiceRender1');
    };

    _renderButton(title, onPress, active) {
        let style = (active) ? styles.activeButtonText : styles.buttonText;

        return (
            <TouchableHighlight style={styles.button} onPress={onPress}>
                <Text style={style}>
                    {title}
                </Text>
            </TouchableHighlight>
        );
    }

    _renderPauseButton(onPress, active) {
        let style = (active) ? styles.activeButtonText : styles.buttonText;
        let title = this.state.paused ? "RESUME" : "PAUSE";
        return (
            <TouchableHighlight style={styles.button} onPress={onPress}>
                <Text style={style}>
                    {title}
                </Text>
            </TouchableHighlight>
        );
    }

    voiceRenderProcessor = async () => {
        console.log({AudioRecorder: AudioRecorder, audioPath: audioPath}, 'RecordScreen -> voiceRenderProcessor2');

        if (!this.state.isRecording) {
            await this.setState({isRecording: true});
            await AudioRecorder.prepareRecordingAtPath(audioPath, {
                SampleRate: 22050,
                Channels: 1,
                AudioQuality: "Low",
                AudioEncoding: "aac"
            });
            await this.setState({isRecording: true, isRecordingFinished: true});
        } else {
            if (this.state.isRecordingFinished) {
                await this.setState({isRecording: false, isRecordingFinished: false});
                await AudioRecorder.stopRecording();
            }
        }
        console.log({AudioRecorder: AudioRecorder}, 'RecordScreen -> voiceRenderProcessor3');
        return AudioRecorder;
    }

    async _pause() {
        if (!this.state.recording) {
            console.warn('Can\'t pause, not recording!');
            return;
        }

        try {
            const filePath = await AudioRecorder.pauseRecording();
            this.setState({paused: true});
        } catch (error) {
            console.error(error);
        }
    }

    async _resume() {
        if (!this.state.paused) {
            console.warn('Can\'t resume, not paused!');
            return;
        }

        try {
            await AudioRecorder.resumeRecording();
            this.setState({paused: false});
        } catch (error) {
            console.error(error);
        }
    }

    async _stop() {
        if (!this.state.recording) {
            console.warn('Can\'t stop, not recording!');
            return;
        }

        this.setState({stoppedRecording: true, recording: false, paused: false});

        try {
            const filePath = await AudioRecorder.stopRecording();

            if (Platform.OS === 'android') {
                this._finishRecording(true, filePath);
            }
            return filePath;
        } catch (error) {
            console.error(error);
        }
    }

    async _play() {
        if (this.state.recording) {
            await this._stop();
        }

        // These timeouts are a hacky workaround for some issues with react-native-sound.
        // See https://github.com/zmxv/react-native-sound/issues/89.
        setTimeout(() => {
            let sound = new Sound(this.state.audioPath, '', (error) => {
                if (error) {
                    console.log('failed to load the sound', error);
                }
            });

            setTimeout(() => {
                sound.play((success) => {
                    if (success) {
                        console.log('successfully finished playing');
                    } else {
                        console.log('playback failed due to audio decoding errors');
                    }
                });
            }, 100);
        }, 100);
    }

    render() {
        return (
            <>
                {/* <TouchableHighlight style={styles.button} onPress={this.voiceRender}>*/}
                {/* <Text>*/}
                {/* {(!this.state.isRecordingFinished) ? `Record!` : `Recording is in session!!`}*/}
                {/* </Text>*/}
                {/* </TouchableHighlight>*/}
                <View style={styles2.container}>
                    <View style={styles2.controls}>
                        {this._renderButton("RECORD", () => {this._record()}, this.state.recording )}
                        {this._renderButton("PLAY", () => {this._play()} )}
                        {this._renderButton("STOP", () => {this._stop()} )}
                        {/* {this._renderButton("PAUSE", () => {this._pause()} )} */}
                        {this._renderPauseButton(() => {this.state.paused ? this._resume() : this._pause()})}
                        <Text style={styles2.progressText}>{this.state.currentTime}s</Text>
                    </View>
                </View>
            </>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 20
    },
    countContainer: {
        alignItems: 'center',
        padding: 10
    },
    countText: {
        color: '#FF00FF'
    },
    red: {
        color: '#ff0000'
    }
})

const styles2 = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#2b608a",
    },
    controls: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    progressText: {
        paddingTop: 50,
        fontSize: 50,
        color: "#fff"
    },
    button: {
        padding: 20
    },
    disabledButtonText: {
        color: '#eee'
    },
    buttonText: {
        fontSize: 20,
        color: "#fff"
    },
    activeButtonText: {
        fontSize: 20,
        color: "#B81F00"
    }

});


